<?php

use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    Log::emergency('Hello', ['used_id =>1']);
    Log::alert('Hello', ['used_id =>1']);
    Log::critical('Hello', ['used_id =>1']);
    Log::error('Hello', ['used_id =>1']);
    Log::warning('Hello', ['used_id =>1']);
    Log::notice('Hello', ['used_id =>1']);
    Log::info('Hello', ['used_id =>1']);
    Log::debug('Hello', ['used_id =>1']);

    Log::channel('abuse')->info('API endpoint abuse', ['used_id =>1']);
    Log::channel('custom')->critical('API endpoint custom', ['used_id =>1']);

});

